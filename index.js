//console.log("hello")
// list of event listenters https://www.javatpoint.com/javascript-events
// text area is kind of different,

//let posts = [];
//let count = 1;

// lets us bypass postman by doing direct communcation with the endpoint. useful for connecting backend and frontend
fetch('https://jsonplaceholder.typicode.com/posts')// fetch makes a request to the given API in the parenthesis
// orange text is user defined
.then((response) => response.json())
.then(data=>{
	console.log(data)
	showPosts(data)
})

let addForm =document.querySelector(`#form-add-post`)

//forms are a wrapper or packager to be sent somewhere for use. reloads the page when used with an event listener kasi no action. it refreshes kasi it does not know where to send the data. so will need to prevent the default response with some code. 
addForm.addEventListener("submit", (e)=>{
// verry very useful
e.preventDefault()
//this is the what we do to stop the default response from happening, useful for forms na walang babatuhan. 
console.log("hello")
//when pushing into array you can push whaterver type of data type. but when you include {} into the push you are specifying the array as an object
posts.push({
	id: count,
	title: document.querySelector(`#txt-title`).value,
	body: document.querySelector(`#txt-body`).value
})
//increment count for post ids
count++ ;
console.log(posts)
// call the show posts function to displays posts
showPosts(posts)
})


//function to show posts instead of in the console
const showPosts = (posts)=> {
	let postEntries = '';
// loops through each post in the array to reassign the string of over and over to write code
	posts.forEach((post)=>{
		postEntries += 
		`
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick= "editPost(${post.id})">Edit</button>
				<button onclick= "deletePost(${post.id})">Delete</button>
			</div>
		`
	})
	// insert the post entries  html code into the empty dive into the div in our html
	document.querySelector(`#div-post-entries`).innerHTML=postEntries;
}

 const editPost = (id) => {
 	// 
 	
 	let title = document.querySelector(`#post-title-${id}`).innerHTML;
 	let body = document.querySelector(`#post-body-${id}`).innerHTML;

 	document.querySelector(`#txt-edit-id`).value=id;
 	document.querySelector(`#txt-edit-title`).value=title;
 	document.querySelector(`#txt-edit-body`).value=body;

 }

// mess around with the hidden and how it passses sa edit after
document.querySelector(`#form-edit-post`).addEventListener(`submit`, (e)=>{
	e.preventDefault()
// for loop for edits
	for (let i=0; i < posts.length; i++){
		//identifies the right posts to edit
		if(document.querySelector(`#txt-edit-id`).value=== posts[i].id.toString()){

			//reassging the ttile and body of the post in the array to the new title and body
			posts[i].title= document.querySelector(`#txt-edit-title`).value;
			posts[i].body= document.querySelector(`#txt-edit-body`).value;
			//calll showPosts again to update the output 
			showPosts(posts)
			// use break to end the loop
			break;

		}
	}
})


// activity delete when you click the delete button.

const deletePost = (id) => {
 	
 	let post = document.querySelector(`#post-${id}`);
 	post.remove();

 }

